import { Storage } from "./storage.js";

const storage = new Storage();


// check create
storage.create({ a: 1, b: 2, c: 7 });
console.log("create worked: ", storage.getAll().length === 1);

// check remove
storage.memory = [];
const s = storage.create({ a: 1, b: 2, c: 3 });
storage.create({ a: 1, b: 2, c: 7 });
const removeFunc = (value) => value.id !== s.id;
storage.remove(removeFunc);
const removeCondition1 = storage.getAll().length === 1;
const removeCondition2 = storage.memory.filter(v => v.id === s.id).length === 0;
const removeCondition = removeCondition1 && removeCondition2;
console.log("remove worked: ", removeCondition);


// check find
storage.memory = [];
storage.create({ a: 1, b: 2, c: 3 });
storage.create({ a: 4, b: 5, c: 6 });
storage.create({ a: 1, b: 2, c: 6 });

const findFunc = (value) => value.a === 1;
const findCondition = storage.find(findFunc).length === 2;
console.log("find worked: ", findCondition);


// check where
storage.memory = [];
storage.create({ a: 1, b: 2, c: 3 });
storage.create({ a: 4, b: 5, c: 6 });
storage.create({ a: 1, b: 2, c: 6 });

console.time('1');
const condition1 = storage.where({ b: 2, a: 1 }).length === 2;
const condition2 = storage.where({ c: 6 }).length === 2;
const condition3 = storage.where({ b: 2, a: 5 }).length === 0;
console.timeEnd('1');

const condition = condition1 && condition2 && condition3;
console.log("where worked: ", condition);

