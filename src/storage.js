import { v4 as uuidv4 } from "uuid";

export class Storage {
  memory;

  constructor(memory = []){
    this.memory = memory;
  }

  create(item) {
    const newItem = {
      ...item,
      id: uuidv4()
    };
    this.memory.push(newItem);
    return newItem;
  }

  find(findFunc) {
    return this.memory.filter(findFunc);
  }

  getAll() {
    return this.memory;
  }

  remove(findFunc) {
    const removed = this.memory.filter(value => !findFunc(value));
    this.memory = this.memory.filter(findFunc);
    return removed;
  }

  where(whereObj) {
    const entries = Object.entries(whereObj);
    const predicate = (value) =>
      entries.every(([eKey, eValue]) => value[eKey] === eValue);
    return this.find(predicate);
  }
}